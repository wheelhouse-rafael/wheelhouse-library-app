export const bookAlreadyRentedError = "Book already rented";
export const bookNotRentedError = "Book is not rented";

export function createInvalidPropertyError(property) {
    return `Pleae provide a valid ${property}`;

}
